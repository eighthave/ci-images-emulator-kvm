
# F-Droid Emulator KVM CI image

Getting an Android emulator running automatically in a container-based CI system is a difficult task.  This project makes it as easy as possible, supporting as many emulator combinations that have proven to run in GitLab-CI.

This Docker image is used in [fdroidclient](https://gitlab.com/fdroid/fdroidclient)'s continuous integration via Gitlab.  It is built on top of Google's [android-emulator-container-scripts](https://github.com/google/android-emulator-container-scripts) This specific image includes stuff that only the client tests need, like emulator images.

As long as Google's _android-emulator-container-scripts_ requires KVM, so will this.